public class App {
    public static void main(String[] args) throws Exception {

        // khởi tạo hai đối tượng person1  person2;

        Person person1 = new Person();
        Person person2 = new Person("thuong", "nguyen", "nam", "032800597", "thuong@.com");

        // in hai đối tượng ra bảng điều khiểnn
        System.out.println("đối tượng person1 : " + person1);
        System.out.println("đối tượng person2 : " + person2);

        System.out.println("đối tượng person1 : " + person1.toString());
        System.out.println("đối tượng person2 : " + person2.getClass());
    }
}
