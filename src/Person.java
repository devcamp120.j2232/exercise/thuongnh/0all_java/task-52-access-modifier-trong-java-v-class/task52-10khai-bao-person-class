public class Person {
    private String FirstName;
    private String LasttName;
    private String Gender;
    private String Contact;
    private String Email;
    // phương thức khởi tạo 
    public Person() {
    }
    public Person(String firstName, String lasttName, String gender, String contact, String email) {
        FirstName = firstName;
        LasttName = lasttName;
        Gender = gender;
        Contact = contact;
        Email = email;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String firstName) {
        FirstName = firstName;
    }
    public String getLasttName() {
        return LasttName;
    }
    public void setLasttName(String lasttName) {
        LasttName = lasttName;
    }
    public String getGender() {
        return Gender;
    }
    public void setGender(String gender) {
        Gender = gender;
    }
    public String getContact() {
        return Contact;
    }
    public void setContact(String contact) {
        Contact = contact;
    }
    public String getEmail() {
        return Email;
    }
    public void setEmail(String email) {
        Email = email;
    }
    public String toString() {
        return "Person [FirstName=" + FirstName + ", LasttName=" + LasttName + ", Gender=" + Gender + ", Contact="
                + Contact + ", Email=" + Email + "]";
    }
    

    

    
}
